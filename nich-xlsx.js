'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _xlsx = require('xlsx');

var _xlsx2 = _interopRequireDefault(_xlsx);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _lodash = require('lodash.has');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Workbook() {
  if (!(this instanceof Workbook)) return new Workbook();
  this.SheetNames = [];
  this.Sheets = {};
}

var nichxlsx = function () {
  function nichxlsx(options) {
    _classCallCheck(this, nichxlsx);

    if ((0, _lodash2.default)(options, 'devMode') && typeof options.devMode === 'boolean') {
      this.devMode = options.devMode;
    } else {
      this.devMode = false;
    };

    if ((0, _lodash2.default)(options, 'disableFinishConsoleLog') && typeof options.disableFinishConsoleLog === 'boolean') {
      this.disableFinishConsoleLog = options.disableFinishConsoleLog;
    } else {
      this.disableFinishConsoleLog = false;
    }

    if ((0, _lodash2.default)(options, 'autoAddExtension') && typeof options.autoAddExtension === 'boolean') {
      this.autoAddExtension = options.autoAddExtension;
    } else {
      this.autoAddExtension = false;
    }

    // default values
    this.filePath = _path2.default.join(__dirname, '/xlsxDefaulFolder');
    this.fileName = 'outputDefaultFileName';
    this.worksheetName = 'XLSX WorkSheet';
    this.mainData = void 0;
    this.headerData = void 0;
  }

  _createClass(nichxlsx, [{
    key: 'datenum',
    value: function datenum(v, date1904) {
      if (date1904) v += 1462;
      var epoch = Date.parse(v);
      return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
    }
  }, {
    key: 'sheet_from_array_of_arrays',
    value: function sheet_from_array_of_arrays(data, opts) {
      var ws = {};
      var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
      for (var R = 0; R != data.length; ++R) {
        for (var C = 0; C != data[R].length; ++C) {
          if (range.s.r > R) range.s.r = R;
          if (range.s.c > C) range.s.c = C;
          if (range.e.r < R) range.e.r = R;
          if (range.e.c < C) range.e.c = C;
          var cell = { v: data[R][C] };
          if (cell.v == null) continue;
          var cell_ref = _xlsx2.default.utils.encode_cell({ c: C, r: R });

          if (typeof cell.v === 'number') cell.t = 'n';else if (typeof cell.v === 'boolean') cell.t = 'b';else if (cell.v instanceof Date) {
            cell.t = 'n';cell.z = _xlsx2.default.SSF._table[14];
            cell.v = this.datenum(cell.v);
          } else cell.t = 's';

          ws[cell_ref] = cell;
        }
      }
      if (range.s.c < 10000000) ws['!ref'] = _xlsx2.default.utils.encode_range(range);
      return ws;
    }

    //
    // Checking methods
    //

  }, {
    key: 'checkPath',
    value: function checkPath(passedFilePath) {
      var filePath = passedFilePath;

      if (typeof filePath === 'undefined') {
        filePath = this.filePath;
      }

      if (typeof filePath !== 'string') {
        throw new Error('Error! filePath is not a string!');
        return 1;
      } else if (typeof filePath === 'string' && filePath.length === 0) {
        throw new Error('Error! filePath is empty!');
        return 1;
      } else if (!_path2.default.isAbsolute(filePath)) {
        throw new Error('Error! filePath is not absolute path! ' + filePath);
        return 1;
      }

      return 0;
    }
  }, {
    key: 'checkName',
    value: function checkName(passedFileName) {
      var fileName = passedFileName;

      if (typeof fileName === 'undefined') {
        fileName = this.fileName;
      }

      if (typeof fileName !== 'string') {
        throw new Error('Error! fileName is not a string!');
        return 1;
      } else if (typeof fileName === 'string' && fileName.length === 0) {
        throw new Error('Error! fileName is empty!');
        return 1;
      }

      return 0;
    }
  }, {
    key: 'prepareAndMakeFile',
    value: function prepareAndMakeFile(filePath, fileName, workBook) {
      var _this = this;

      var fullFileName = fileName;

      if (this.autoAddExtension) {
        fullFileName += '.xlsx';
      }

      var newFilePath = _path2.default.join(filePath, fullFileName);

      if (this.devMode) console.log('CheckFile| FullPath: ' + newFilePath);

      (0, _mkdirp2.default)(filePath, function (err) {
        if (err) {
          throw new Error('Path ERROR! prepareAndMakeFile');
          return 1;
        }
        var createStream = _fs2.default.createWriteStream(newFilePath);
        createStream.end();

        _xlsx2.default.writeFile(workBook, newFilePath);

        if (!_this.disableFinishConsoleLog) console.log('XLSX File Done [' + newFilePath + ']');

        return 0;
      });
    }

    // MAIN METHODS
    /**
     * Set path for file
     * @param {string} filePath [global path to the file]
     */

  }, {
    key: 'setFilePath',
    value: function setFilePath(filePath) {
      if (this.checkPath(filePath) == 1) {
        // something bad happened
        return 1;
      }

      this.filePath = filePath;
      return 0;
    }

    /**
     * Set name for file
     * @param {string} fileName [file name =)]
     */

  }, {
    key: 'setFileName',
    value: function setFileName(fileName) {
      if (typeof fileName !== 'string') {
        throw new Error('Error! fileName is not a string!');
        return 1;
      }

      this.fileName = fileName;
      return 0;
    }

    /**
     * Set workSheetName for your xlsx file in Excel
     * @param {string} workSheetName [sheet name]
     */

  }, {
    key: 'setWorkSheetName',
    value: function setWorkSheetName(workSheetName) {
      if (typeof workSheetName !== 'string') {
        throw new Error('Error! workSheetName is not a string!');
        return 1;
      }
      if (typeof workSheetName === 'string' && workSheetName.length === 0) {
        throw new Error('Error! workSheetName is empty!');
        return 1;
      }

      this.workSheetName = workSheetName;
      return 0;
    }

    /**
     * Set main data in your xlsx file into two-dimensional massive
     * @param {array} mainData [main data into two-dimensional array]
     */

  }, {
    key: 'setMainData',
    value: function setMainData(mainData) {
      if (!Array.isArray(mainData)) {
        throw new Error('Error! mainData is not an array!');
        return 1;
      }

      this.mainData = mainData;
      return 0;
    }

    /**
     * Set header data into one-dimensional massive
     * @param {array} headerData [header one-dimensional array]
     */

  }, {
    key: 'setHeaderData',
    value: function setHeaderData(headerData) {
      if (!Array.isArray(headerData)) {
        throw new Error('Error! headerData is not an array!');
        return 1;
      }

      this.headerData = headerData;
      return 0;
    }

    /**
     * Clear all class data
     */

  }, {
    key: 'clearAllData',
    value: function clearAllData() {
      this.filePath = '';
      this.fileName = '';
      this.worksheetName = '';
      this.mainData = void 0;
      this.headerData = void 0;
    }

    /**
     * Make new xlsx file
     */

  }, {
    key: 'makeFile',
    value: function makeFile() {
      if (typeof this.headerData !== 'undefined') {
        this.mainData.unshift(this.headerData);
      }

      if (typeof this.mainData === 'undefined') {
        throw new Error('You don\'t have mainData parameter!');
      }

      this.checkPath();
      this.checkName();

      var workBook = new Workbook();
      var workSheet = this.sheet_from_array_of_arrays(this.mainData);

      // add worksheet to workbook
      workBook.SheetNames.push(this.workSheetName);
      workBook.Sheets[this.workSheetName] = workSheet;

      // write file
      this.prepareAndMakeFile(this.filePath, this.fileName, workBook);
    }
  }]);

  return nichxlsx;
}();

exports.default = nichxlsx;