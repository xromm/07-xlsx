'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _xlsx = require('xlsx');

var _xlsx2 = _interopRequireDefault(_xlsx);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _nichXlsx = require('./nich-xlsx');

var _nichXlsx2 = _interopRequireDefault(_nichXlsx);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _nichXlsx2.default;

/* testing component nich-xlsx module */

if (false) {
  /**
   * Additional options for package
   * @param {boolean} devMode [enable or disable developer mode,
   *                          default value - false]
   * @param {boolean} disableFinishConsoleLog [on/of console log after writing file
   *                                          default value - false]
   * @param {boolean} autoAddExtension [autoAdding .xlsx extension
   *                                   default value - false]
   */
  var options = {
    // devMode: true,
    // disableFinishConsoleLog: true,
    autoAddExtension: true
  };

  // init class for xlsx files
  var folder = new _nichXlsx2.default(options);

  // init data for file
  var filePath = _path2.default.join(__dirname, 'folderName'); // add file in folder folderName
  var fileName = 'testName'; // fileName without .xlsx extensions
  var workSheetName = 'sheetName';
  var mainData = [['d11', 'd12', 'd13'], ['d21', 'd22', 'd23'], ['d31', 'f32', 'f33']];
  var headerData = ['header 1', 'header 2', 'header 3'];

  try {
    // passing data
    folder.setFilePath(filePath);
    folder.setFileName(fileName);
    folder.setWorkSheetName(workSheetName);
    folder.setMainData(mainData);
    folder.setHeaderData(headerData);

    // make new xlsx file
    folder.makeFile();

    // clear all data
    folder.clearAllData();

    // passinf olmost the same data
    folder.setFileName(fileName + 'SecondFile');
    folder.setFilePath(filePath);
    folder.setMainData(mainData);

    // make another xlsx file
    folder.makeFile();
  } catch (error) {
    console.log('Throw ERROR! ' + error.name + ' | ' + error.message);
  }
}