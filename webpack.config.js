var path = require('path');
var webpack = require("webpack");

module.exports = {
  devtool: 'source-map',
  entry: [
    './source/index.js'
  ],
  output: {
    path: __dirname,
    filename: 'index.js'
  },
  resolve: {
    moduleDirectories: ['node_modules'],
    extensions: ['', '.js', '.jsx']
  },
  resolveLoader: {
    modulesDirecrories: ['node_modules'],
    moduleTemplates: ['*-loader', '*'],
    extenstions: ['', 'js']
  },
  module: {
    loaders: [ //добавили babel-loader
      {
        exclude: [
          path.resolve(__dirname, "node_modules"),
        ],
        loaders: ['babel-loader'],
        include: [
          path.resolve(__dirname, "source"),
        ],
        test: /\.js?$/,
        plugins: ['transform-runtime'],
      }
    ]
  },

  // solve problem with "Error: Cannot resolve module 'fs'"
  target: 'node',
  node: {
    fs: "empty",
    __dirname: false,
  },
  externals: [
    {
      './jszip': 'jszip',
      './cptable': 'var cptable',
      'Mkdirp': 'mkdirp',
      'Xlsx': 'xlsx'
    }
  ]
}
