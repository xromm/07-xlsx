Based on XLSX npm module.

#Installation

```
npm i nich-xlsx
```

#Running the simple index.js file via terminal
1. Make index.js file.
2. Copy and paste code from the simple exapmle on the bottom of this page into the new index.js file.
3. Save file.
4. Open terminal into the current folder with your index.js file.
on Windows [Win+R > write cmd > Enter]
on Mac [CMD + Space > write terminal > Enter]
5. Run **npm i nich-xlsx** command.
6. Then run **node index.js** command.
7. In your terminal you should see console logs =) I hope they would be successful.
8. In your current folder should be new 'folderName' folder and in it should be testName.xlsx file with default data.

#Class methods:

```
 // Set path for file
 // @param {string} filePath [global path to the file]
setFilePath(filePath);
```

```
// Set name for file
// @param {string} fileName [file name =)]
setFileName(fileName);
```

```
// Set workSheetName for your xlsx file in Excel
// @param {string} workSheetName [sheet name]
setWorkSheetName(workSheetName);
```

```
// Set main data in your xlsx file into two-dimensional massive
// @param {array} mainData [main data into two-dimensional array]
setMainData(mainData);
```

```
// Set header data into one-dimensional massive
// @param {array} headerData [header one-dimensional array]
setHeaderData(headerData);
```

```
// Clear all class data
clearAllData();
```

```
// Make new xlsx file
makeFile();
```

#Index.js Simple Example File

```js
var nichxlsx = require('nich-xlsx');
var path = require('path');

// Additional options for package
// @param {boolean} devMode [enable or disable developer mode,
//   default value - false]
// @param {boolean} disableFinishConsoleLog [on/of console log after writing file
//   default value - false]
// @param {boolean} autoAddExtension [autoAdding '.xlsx' extension
//   default value - false]

var options = {
  // devMode: true,
  // disableFinishConsoleLog: true,
  autoAddExtension: true,
}

// init class for xlsx files
var folder = new nichxlsx.default(options); // passing options to the xlsx class

// init data for file
var filePath = path.join(__dirname, 'folderName'); // add file in folder 'folderName'
var fileName = 'testName'; // fileName without '.xlsx' extension
var workSheetName = 'sheetName'; // name for sheet name in Excel
var mainData = [['d11','d12','d13'],['d21','d22','d23'],['d31','f32','f33']]; // main data
var headerData = ['header 1', 'header 2', 'header 3']; // headers for main data columns

try {
  // passing data for the file
  folder.setFilePath(filePath);
  folder.setFileName(fileName);
  folder.setWorkSheetName(workSheetName);
  folder.setMainData(mainData);
  folder.setHeaderData(headerData);

  // make new xlsx file
  folder.makeFile();

  // clear all data
  folder.clearAllData();

  // passing almost the same data
  folder.setFileName(fileName + 'SecondFile'); // change file name for next file
  folder.setFilePath(filePath);
  folder.setMainData(mainData); // only mainData without headerData

  // make another xlsx file
  folder.makeFile();
} catch (error) {
  console.log('Throw ERROR!' + error.name + ' | ' + error.message);
}
```
-----------------------------
If you have Errors like this:
#Error: Cannot resolve module 'fs'
so you need to modify your webpack config file:
```js
...
target: 'node',
node: {
	fs: "empty",
	__dirname: false,
}
...
```

If you have problem with jszip or mkdirp, then you need also add these lines:
```js
...
externals: [
	{
		'./jszip': 'jszip',
		'./cptable': 'var cptable',
		'Mkdirp': 'mkdirp',
		'Xlsx': 'xlsx'
	}
]
...
```
