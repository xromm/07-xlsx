module.exports = function(config) {
  config.set({
    files: [
      './source/**/*.spec.js'
    ],
    preprocessors: {
      './source/**/*.spec.js': ['webpack', 'sourcemap']
    },

    webpack: {
      devtool: 'inline-source-map',
      module: {
        loaders: [
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel-loader',

          query: {
            plugins: ['transform-runtime'],
            presets: ['es2015', 'stage-0'],
          }
        }
        ]
      },
      resolve: {
        extensions: ['', '.js']
      },

      // Solving error problems
      node: {
        fs: "empty"
      },
      externals: [
        {
          './cptable': 'var cptable'
        }
      ]
    },
    webpackMiddleware: {
      stats: 'errors-only'
    },

    // Add any browsers here
    browsers: ['PhantomJS2'],
    frameworks: ['jasmine'],

    // The entry point for our test suite
    basePath: '',
    autoWatch: false,
    colors: true,

    client: {
      // log console output in our test console
      captureConsole: true
    },

    port: 9876,
    logLevel: config.LOG_ERROR,

    reporters: ['spec'],
    specReporter: {
      maxLogLines: 5, // limit number of lines logged per test
      suppressErrorSummary: true, // do not print error summary
      suppressFailed: false, // do not print information about failed tests
      suppressPassed: false, // do not print information about passed tests
      suppressSkipped: true, // do not print information about skipped tests
      showSpecTiming: true // print the time elapsed for each spec
    },
    plugins: [
      "karma-spec-reporter",
      "karma-webpack",
      "karma-sourcemap-loader",
      "karma-jasmine",
      "karma-phantomjs-launcher",
      "karma-phantomjs2-launcher"
    ],

    singleRun: true, // exit after tests have completed
  });
};
