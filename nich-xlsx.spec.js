'use strict';

var _nichXlsx = require('./nich-xlsx');

var _nichXlsx2 = _interopRequireDefault(_nichXlsx);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function setup() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  // init class for xlsx files
  var folder = new _nichXlsx2.default(options);

  // init data for file
  var filePath = _path2.default.join(__dirname, data.folderName); // add file in folder folderName
  var fileName = data.fileName; // fileName without .xlsx extensions
  var workSheetName = data.sheetName;
  var mainData = data.mainData;
  var headerData = data.headerData;

  console.log('' + filePath);
  try {
    // passing data
    folder.setFilePath(filePath);
    folder.setFileName(fileName);
    folder.setWorkSheetName(workSheetName);
    folder.setMainData(mainData);
    folder.setHeaderData(headerData);

    // // make new xlsx file
    folder.makeFile();

    return 0;
  } catch (error) {
    console.log('Throw ERROR! ' + error.name + ' | ' + error.message);
    return 1;
  }
}

describe('TEST', function () {
  it('1 to 1', function () {
    console.log('TEST');
    expect(1).toBe(1);
  });

  it('should behave...', function () {
    var options = {
      devMode: true,
      // disableFinishConsoleLog: true,
      autoAddExtension: true
    };
    var data = {
      folderName: 'testNamePath',
      fileName: 'testName',
      sheetName: 'sheetName',
      mainData: [['d11', 'd12', 'd13'], ['d21', 'd22', 'd23'], ['d31', 'f32', 'f33']],
      headerData: ['header 1', 'header 2', 'header 3']
    };

    expect(setup(options, data)).toEqual(0);
  });
});