import nichxlsx from './nich-xlsx';
import path from 'path';

function setup(options = {}, data = {}) {
  // init class for xlsx files
  const folder = new nichxlsx(options);

  // init data for file
  const filePath = path.join(__dirname, data.folderName); // add file in folder folderName
  const fileName = data.fileName; // fileName without .xlsx extensions
  const workSheetName = data.sheetName;
  const mainData = data.mainData;
  const headerData = data.headerData;

  console.log(`${filePath}`);
  try {
    // passing data
    folder.setFilePath(filePath);
    folder.setFileName(fileName);
    folder.setWorkSheetName(workSheetName);
    folder.setMainData(mainData);
    folder.setHeaderData(headerData);

    // // make new xlsx file
    folder.makeFile();

    return 0;
  } catch (error) {
    console.log(`Throw ERROR! ${error.name} | ${error.message}`);
    return 1;
  }
}

describe('TEST', () => {
  it('1 to 1', () => {
    console.log('TEST');
    expect(1).toBe(1);
  });

  it('should behave...', () => {
    const options = {
      devMode: true,
      // disableFinishConsoleLog: true,
      autoAddExtension: true,
    };
    const data = {
      folderName: 'testNamePath',
      fileName: 'testName',
      sheetName: 'sheetName',
      mainData: [
        ['d11','d12','d13'],
        ['d21','d22','d23'],
        ['d31','f32','f33' ]
      ],
      headerData: ['header 1', 'header 2', 'header 3']
    };

    expect(setup(options, data)).toEqual(0);
  });
});