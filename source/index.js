import XLSX from 'xlsx';
import path from 'path';
import nichxlsx from './nich-xlsx';

export default nichxlsx;

/* testing component nich-xlsx module */
if (false) {
  /**
   * Additional options for package
   * @param {boolean} devMode [enable or disable developer mode,
   *                          default value - false]
   * @param {boolean} disableFinishConsoleLog [on/of console log after writing file
   *                                          default value - false]
   * @param {boolean} autoAddExtension [autoAdding .xlsx extension
   *                                   default value - false]
   */
  const options = {
    // devMode: true,
    // disableFinishConsoleLog: true,
    autoAddExtension: true,
  }


  // init class for xlsx files
  const folder = new nichxlsx(options);

  // init data for file
  const filePath = path.join(__dirname, 'folderName'); // add file in folder folderName
  const fileName = 'testName'; // fileName without .xlsx extensions
  const workSheetName = 'sheetName';
  const mainData = [['d11','d12','d13'],['d21','d22','d23'],['d31','f32','f33']];
  const headerData = ['header 1', 'header 2', 'header 3'];

  try {
    // passing data
    folder.setFilePath(filePath);
    folder.setFileName(fileName);
    folder.setWorkSheetName(workSheetName);
    folder.setMainData(mainData);
    folder.setHeaderData(headerData);

    // make new xlsx file
    folder.makeFile();

    // clear all data
    folder.clearAllData();

    // passinf olmost the same data
    folder.setFileName(`${fileName}SecondFile`);
    folder.setFilePath(filePath);
    folder.setMainData(mainData);

    // make another xlsx file
    folder.makeFile();
  } catch (error) {
    console.log(`Throw ERROR! ${error.name} | ${error.message}`);
  }
}